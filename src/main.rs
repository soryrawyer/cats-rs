use std::collections::HashMap;

fn main() {
    println!("Hello, world!");
    println!("my identity is: {}", identity(1));
    println!("my identity is: {}", identity(String::from("pffff")));
    println!("my identity is: {}", identity("pffff"));

    let int_to_int = compose(&(identity as fn(u16) -> u16), &(identity as fn(u16) -> u16));
    println!("composed identities? {}", int_to_int(1));

    let int_string_int = compose(
        &(int_to_string as fn(u32) -> String),
        &(string_to_int as fn(String) -> u32),
    );
    println!(
        "composed int to string back to int: {}",
        int_string_int(123)
    );

    let mut add1_memo = memoize(&(add1 as fn(i64) -> i64));
    println!("add1: {}", add1_memo(4));
    println!("add1, but memoized: {}", add1_memo(4));
}

fn identity<A>(a: A) -> A {
    a
}

fn compose<A, B, C>(f: &'static fn(A) -> B, g: &'static fn(B) -> C) -> impl Fn(A) -> C {
    move |a| g(f(a))
}

fn int_to_string(x: u32) -> String {
    x.to_string()
}

fn string_to_int(x: String) -> u32 {
    x.parse::<u32>().unwrap()
}

// lol this is so bad. I should probably replace all these clones and
// copies with lifetimes?
fn memoize<A, B>(f: &'static fn(A) -> B) -> impl FnMut(A) -> B
where
    A: std::cmp::Eq + std::hash::Hash + std::clone::Clone,
    B: std::clone::Clone + std::marker::Copy,
{
    let mut results: HashMap<A, B> = HashMap::new();
    move |a| {
        if let Some(something) = results.get(&a) {
            println!("finding memo (pronounced like 'finding nemo')");
            something.clone()
        } else {
            println!("aaah, haven't seen that one before");
            let result: B = f(a.clone());
            results.insert(a, result.clone());
            result
        }
    }
}

fn add1(x: i64) -> i64 {
    x + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn composition_respects_identity() {
        let string_ident = compose(
            &(int_to_string as fn(u32) -> String),
            &(identity as fn(String) -> String),
        );
        let ident_string = compose(
            &(identity as fn(u32) -> u32),
            &(int_to_string as fn(u32) -> String),
        );
        assert_eq!(string_ident(4321), ident_string(4321));
    }
}
